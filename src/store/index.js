import Vue from 'vue'
import Vuex from 'vuex'

import { vuexfireMutations } from 'vuexfire'

import * as auth from './modules/auth'
import * as admin from './modules/admin'
import * as employee from './modules/employee'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: vuexfireMutations,
  actions: {},
  modules: {
    auth,
    admin,
    employee,
  },
})
