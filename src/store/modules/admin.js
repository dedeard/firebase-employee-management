import firebase from 'firebase/app'
import { firestoreAction } from 'vuexfire'

const db = firebase.firestore()

const adminRef = db.collection('admin')

export const state = () => ({
  admin: [],
})

export const actions = {
  bindAdmin: firestoreAction(({ bindFirestoreRef }) => {
    return bindFirestoreRef('admin', adminRef)
  }),
  unbindAdmin: firestoreAction(({ unbindFirestoreRef }) => {
    unbindFirestoreRef('admin')
  }),
  destroyAdmin(conntex, email) {
    return adminRef.doc(email).delete()
  },
  createAdmin(conntex, { email, name }) {
    return adminRef.doc(email).set({ name })
  },
}
