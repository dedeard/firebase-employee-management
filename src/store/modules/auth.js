import firebase from 'firebase/app'

const auth = firebase.auth()
const db = firebase.firestore()
const provider = new firebase.auth.GoogleAuthProvider()

export const state = () => ({
  user: null,
  role: '',
})

export const getters = {
  isLogin(state) {
    return !!state.user
  },
  auth(state) {
    return state.user
  },
  role(state) {
    return state.role
  },
  isSuperuser(state) {
    return state.role === 'superuser'
  },
}

export const actions = {
  signInWithGoogle({ commit, dispatch }) {
    return auth
      .signInWithPopup(provider)
      .then((result) => {
        return dispatch('checkRole', result.user)
      })
      .then((res) => {
        commit('setAuth', res)
        return res
      })
  },
  register({ commit, dispatch }, { email, password }) {
    return auth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        return dispatch('checkRole', result.user)
      })
      .then((res) => {
        commit('setAuth', res)
        return res
      })
  },
  login({ commit, dispatch }, { email, password }) {
    return auth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        return dispatch('checkRole', result.user)
      })
      .then((res) => {
        commit('setAuth', res)
        return res
      })
  },
  forgetPassword(context, email) {
    return auth.sendPasswordResetEmail(email)
  },
  logout({ commit }) {
    return auth.signOut().then((res) => {
      commit('setAuth', { user: null, role: '' })
      return res
    })
  },
  checkRole({ dispatch }, user) {
    return new Promise((resolve, reject) => {
      db.collection('superuser')
        .doc(user.email)
        .get()
        .then((doc) => {
          if (doc.exists) {
            resolve({ user, role: 'superuser' })
          } else {
            return db.collection('admin').doc(user.email).get()
          }
        })
        .then((doc) => {
          if (doc.exists) {
            resolve({ user, role: 'admin' })
          } else {
            dispatch('logout')
            reject(new Error('No permission for your account!'))
          }
        })
        .catch(reject)
    })
  },
}

export const mutations = {
  setAuth(state, { user, role }) {
    state.user = user || null
    state.role = role || ''
  },
}
