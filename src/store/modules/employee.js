import firebase from 'firebase/app'
import { firestoreAction } from 'vuexfire'

const db = firebase.firestore()

const employeesRef = db.collection('employees')

export const state = () => ({
  employees: [],
})

export const getters = {
  employees(state) {
    return state.employees
  },
}

export const actions = {
  bindEmployees: firestoreAction(({ bindFirestoreRef }) => {
    return bindFirestoreRef('employees', employeesRef)
  }),
  unbindEmployees: firestoreAction(({ unbindFirestoreRef }) => {
    unbindFirestoreRef('employees')
  }),
  destroyEmployee(conntex, id) {
    return employeesRef.doc(id).delete()
  },
  createEmployee(conntex, payload) {
    return employeesRef.add(payload)
  },
  updateEmployee(conntex, { id, data }) {
    return employeesRef.doc(id).update(data)
  },
}
