import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

import DefaultLayout from '@/components/layouts/Default'
import AuthLayout from '@/components/layouts/Auth'

import HomePage from '@/views/Home'
import AdminPage from '@/views/Admin'
import EmployeePage from '@/views/Employee'

import LoginPage from '@/views/Login'
import RegisterPage from '@/views/Register'
import ForgetPasswordPage from '@/views/ForgetPassword'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    redirect: { name: 'Home' },
  },
  {
    path: '/app',
    component: DefaultLayout,
    children: [
      {
        path: 'home',
        name: 'Home',
        component: HomePage,
      },
      {
        path: 'admin',
        name: 'Admin',
        component: AdminPage,
        meta: {
          superuser: true,
        },
      },
      {
        path: 'employee',
        name: 'Employee',
        component: EmployeePage,
      },
    ],
  },
  {
    path: '/auth',
    component: AuthLayout,
    redirect: { name: 'Login' },
    children: [
      {
        path: 'login',
        name: 'Login',
        component: LoginPage,
      },
      {
        path: 'register',
        name: 'Register',
        component: RegisterPage,
      },
      {
        path: 'forget',
        name: 'Forget Password',
        component: ForgetPasswordPage,
      },
    ],
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'is-active',
  routes,
})

router.beforeEach((to, from, next) => {
  const paths = to.path.split('/').filter((el) => el)
  if (!store.getters.isLogin && paths && paths[0] != 'auth') {
    next({ name: 'Login' })
  } else if (store.getters.isLogin && paths && paths[0] === 'auth') {
    next({ name: 'Home' })
  } else if (to.meta.superuser && !store.getters.isSuperuser) {
    next({ name: 'Home' })
  } else {
    next()
  }
})

export default router
