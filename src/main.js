import './firebase'
import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import plugins from './plugins'
import firebase from 'firebase/app'
import i18n from './i18n'

Vue.config.productionTip = false

Vue.use(plugins)

let vue = null
const check = firebase.auth().onAuthStateChanged(async (user) => {
  if (user) {
    await store
      .dispatch('checkRole', user)
      .then((res) => {
        return store.commit('setAuth', res)
      })
      .catch(() => {
        // console.log(e)
      })
  }
  if (!vue) {
    vue = new Vue({
      router,
      store,
      i18n,
      render: (h) => h(App),
    }).$mount('#app')
  }
  check()
})
