import { VLazyImagePlugin } from 'v-lazy-image'
import Buefy from 'buefy'
import VueToast from 'vue-toast-notification'

import '@mdi/font/css/materialdesignicons.css'
import 'buefy/dist/buefy.css'
import 'vue-toast-notification/dist/theme-sugar.css'

const Plugins = {
  install(Vue) {
    Vue.use(VLazyImagePlugin)
    Vue.use(VueToast)
    Vue.use(Buefy)
  },
}

export default Plugins
